package com.pawelbanasik.sda_012_ap_login_app;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Main2Activity extends AppCompatActivity {

    @BindView(R.id.text_view_one)
    protected TextView textViewOne;

    public static final String USER_INPUT_LOGIN = "input";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        ButterKnife.bind(this);

        textViewOne.setText("Welcome " + getIntent().getStringExtra(USER_INPUT_LOGIN));
    }
}
