package com.pawelbanasik.sda_012_ap_login_app;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    UserService userService = new UserService();

    @BindView(R.id.button_one)
    protected Button buttonOne;

    @BindView(R.id.edit_text_one)
    protected EditText editTextOne;

    @BindView(R.id.edit_text_two)
    protected EditText editTextTwo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.button_one)
    public void buttonClick(View v) {

        // moja bardziej prosta metoda pisania
//        String login = "admin";
//        String password = "admin";
//        if (editTextOne.getText().toString().equals(login) && editTextTwo.getText().toString().equals(password)){

        // metoda pisania przez Adriana czyli uzywa osobnej klasy jaka jest UserService i duzo zmiennych
        final String nameInput = editTextOne.getText().toString();
        final String passwordInput = editTextTwo.getText().toString();

        if (userService.checkUser(nameInput, passwordInput)) {

            Context context = getApplicationContext();
            CharSequence text = "You logged in.";
            int duration = Toast.LENGTH_SHORT;
            Toast toast = Toast.makeText(context, text, duration);
            toast.show();

            Intent intent = new Intent(getApplicationContext(), Main2Activity.class);

            // zla praktyka (musimy robic tak zeby kodu nie trzeba bylo zmieniac)
//          intent.putExtra("input", editTextOne.getText().toString());

            intent.putExtra(Main2Activity.USER_INPUT_LOGIN, editTextOne.getText().toString());

            startActivity(intent);

        } else {

            Context context = getApplicationContext();
            CharSequence text = "Invalid username or password.";
            int duration = Toast.LENGTH_SHORT;

            Toast toast = Toast.makeText(context, text, duration);
            toast.show();

        }
    }
}
